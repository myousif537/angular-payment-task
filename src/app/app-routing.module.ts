import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    loadChildren: () =>
      import(`./Modules/Auth/auth.module`).then((auth) => auth.AuthModule),
  },

  {
    path: 'post',
    loadChildren: () =>
      import(`./Modules/Post/post.module`).then((post) => post.PostModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
