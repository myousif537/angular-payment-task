import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../material.module'
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'



import {ValidationMessageDialogComponent,SharedService


} from './index';

@NgModule({
  declarations: [

    ValidationMessageDialogComponent


  ],
  imports: [

    HttpClientModule,
    FormsModule,
    CommonModule,
    MaterialModule,





  ],
  providers: [SharedService],
  exports:[ValidationMessageDialogComponent],
  entryComponents: [ValidationMessageDialogComponent]
})
export class TaskCommonModule { }
