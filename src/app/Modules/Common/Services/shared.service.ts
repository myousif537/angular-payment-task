import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ValidationMessageDialogComponent } from '../Components/validation-message-dialog/validation-message-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  constructor(private dialog: MatDialog, private snackBar:MatSnackBar) {}

  message(
    icon: string,
    title: string,
    message: string,
    width?: string,
    cancelButton?: boolean
  ): Observable<any> {
    let defaultWidth: string = '500px';
    const dialogRef = this.dialog.open(ValidationMessageDialogComponent, {
      width: width ? width : defaultWidth,
      data: {
        icon: icon,
        title: title,
        message: message,
        cancelButton: cancelButton ? true : false,
      },
    });
    return dialogRef.afterClosed();
  }

  showSnackBar(message:string){
    this.snackBar.open(message,'OK', {duration:2000});
  }
}
