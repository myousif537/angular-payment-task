import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostComponent } from './index';

const routes: Routes = [
  {
    path: '',
    children: [


      {
        path: '',
        children: [
          {
            path: '',
            component: PostComponent,
          },

          {
            path: 'id',
            component: PostComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostRoutingModule {}

export const routedComponents = [PostComponent];
