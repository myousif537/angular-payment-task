export class Post{
  id:number = 0;
  creditCardNumber:string = '';
  cardHolder:string = '';
  expirationDate: Date = new Date();
  securityCode:string = '';
  amount:number;
}
