import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../../Models/post.model';
import { PostService } from '../../Services/post.service';
import { takeUntil } from 'rxjs/operators';
import { SharedService } from '../../../Common/Services/shared.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit, OnDestroy {
  post: Post = new Post();

  constructor(
    private postService: PostService,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {}

  postPayment() {
    this.postService
      .pay(this.post)
      .pipe(takeUntil(this.postService.unSubscriber))
      .subscribe(
        (res) => {
          console.log(res);
        },
        (err) => {
          this.sharedService.message('warning', 'Error', err.error.error);
          console.log('error');
        },
        () => {
          this.sharedService.showSnackBar('Payment posted successfully...');
        }
      );
  }

  ngOnDestroy() {
    this.postService.unSubscriber.unsubscribe();
  }
}
