import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, timeout, retry, catchError } from 'rxjs/operators';
import { Post } from '../Models/post.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private http: HttpClient) {}

  public unSubscriber: Subject<any> = new Subject();

  //I'm using random api so make sure that' not working because it's fake api

  pay(dataModel: Post): Observable<any> {
    return this.http.post('https://test.pay.com/pay', dataModel).pipe(
      timeout(60000),
      map((res: Post[]) => {
        if (!res && !res.length) return;
        console.log(res);

        return res;
      }),
      retry(0)
    );
  }
}
