import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../material.module'
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { PostRoutingModule, routedComponents} from './post-routing.module';
import { TaskCommonModule } from '../Common/task-common.module';



import {
PostService

} from './index';

@NgModule({
  declarations: [
    routedComponents,



  ],
  imports: [
    PostRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    MaterialModule,
    TaskCommonModule





  ],
  providers: [PostService],
  entryComponents: []
})
export class PostModule { }
