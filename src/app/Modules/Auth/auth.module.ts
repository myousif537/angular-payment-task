import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../material.module'
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { AuthRoutingModule, routedComponents} from './auth-routing.module';
import { TaskCommonModule } from '../Common/task-common.module'


@NgModule({
  declarations: [
    routedComponents,



  ],
  imports: [
    AuthRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    MaterialModule,
    TaskCommonModule





  ],
  providers: [],
  entryComponents: []
})
export class AuthModule { }
