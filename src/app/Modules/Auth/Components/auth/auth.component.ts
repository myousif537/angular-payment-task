import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../../Common/Services/shared.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  user: User = new User();

  forms: NgForm[] = [];
  private _form: NgForm;
  @ViewChild('form', { static: true })
  set form(value: NgForm) {
    this._form = value;
    if (this._form) {
      this.forms.push(this.form);
    }
  }
  get form(): NgForm {
    return this._form;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private shared: SharedService
  ) {}

  ngOnInit(): void {}

  login() {
    if(this.user.email !== 'myousif537@gmail.com'){
      this.shared.message('warning','Error', 'Email is incorrect...');
      return;
    }
    else if(this.user.password !== '123456'){
      this.shared.message('warning','Error', 'Password is incorrect...');
      return;
    }
    this.router.navigate(['/post'],{relativeTo:this.route});
    this.shared.showSnackBar('Welcome to our system...');
  }
}

class User {
  email: String = '';
  password: String = '';
}
